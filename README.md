# cpp-workshop-oven

![preview](.doc/screenshot.jpg)

## Prerequisites

You should have [SDL](http://libsdl.org/) installed.

You also need an IDE with CMake support.

## Simulator

Build and launch the target `oven_simulator`

## Development

Edit your application files in `src/lib/oven` and integrate them in the
simulator at `src/bin/oven_simulator`.