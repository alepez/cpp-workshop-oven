#pragma once

#include <SDL_keyboard.h>
#include <SDL_render.h>
#include <SDL_video.h>

#include <array>
#include <chrono>
#include <map>

namespace simulator {

struct SevenSegmentsDisplay {
    std::array<bool, 8> segments{};
};

struct Resistance {
    bool on{};
};

struct Buzzer {
    bool on{};
};

struct Button {
    bool pressed{};
};

using Buttons = std::array<Button, 3>;

struct TemperatureSensor {
    double celsius{20.0};
};

struct SimulatorInputs {
    Buttons buttons{};
    TemperatureSensor temperature_sensor{};
};

using Displays = std::array<SevenSegmentsDisplay, 3>;

struct SimulatorOutputs {
    Displays displays{};
    Resistance resistance{};
    Buzzer buzzer{};
};

class Simulator {
  public:
    Simulator();
    ~Simulator();
    Simulator(const Simulator&) = delete;
    Simulator(Simulator&&) = delete;
    Simulator& operator=(const Simulator&) = delete;
    Simulator& operator=(Simulator&&) = delete;

    void update();

    uint32_t* get_registers();

    bool should_quit() const;

  private:
    void set_button_state(SDL_Keycode key_code, bool pressed);
    void sync_input_registers();
    void sync_output_registers();
    void sync_time_registers();
    void render();
    void handle_events();
    void update_temperature();

    SDL_Window* win_{};
    SDL_Renderer* renderer_{};
    bool is_quit_requested_{};
    std::chrono::system_clock::time_point start_time_;

    std::array<uint32_t, 3> registers_{};

    SimulatorInputs inputs_;
    SimulatorOutputs outputs_;
};

uint32_t celsius_to_raw_temperature(double temperature_celsius);

void register_to_seven_seg_display(
    uint32_t r,
    SevenSegmentsDisplay& display,
    uint32_t display_id);

void draw(
    SDL_Renderer& renderer,
    const SevenSegmentsDisplay& display,
    uint32_t display_id);

void draw(SDL_Renderer& renderer, const Buzzer& buzzer);

void draw(SDL_Renderer& renderer, const Resistance& resistance);

void draw(SDL_Renderer& renderer, const Buttons& buttons);

void draw(SDL_Renderer& renderer, const TemperatureSensor& temperature_sensor);

}  // namespace simulator