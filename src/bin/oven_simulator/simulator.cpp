#include "simulator.hpp"

#include <SDL.h>
#include <fmt/core.h>
#include <spdlog/spdlog.h>

namespace simulator {

struct SimulatorError: std::exception {};

Simulator::Simulator() {
    static const int width = 512;
    static const int height = 512;

    auto sdl_init_result = SDL_Init(SDL_INIT_VIDEO);

    if (sdl_init_result != 0) {
        throw SimulatorError{};
    }

    if (SDL_CreateWindowAndRenderer(width, height, 0, &win_, &renderer_) != 0) {
        throw SimulatorError{};
    }

    if ((win_ == nullptr) || (renderer_ == nullptr)) {
        throw SimulatorError{};
    }

    start_time_ = std::chrono::system_clock::now();
}

Simulator::~Simulator() {
    SDL_DestroyRenderer(renderer_);
    SDL_DestroyWindow(win_);
    SDL_Quit();
}

void Simulator::update() {
    SDL_Delay(20);
    sync_output_registers();
    sync_time_registers();
    update_temperature();
    render();
    handle_events();
    sync_input_registers();
}

void Simulator::handle_events() {
    SDL_Event event;

    while (SDL_PollEvent(&event) == 1) {
        switch (event.type) {
            case SDL_QUIT:
                is_quit_requested_ = true;
                break;
            case SDL_KEYDOWN:
                set_button_state(event.key.keysym.sym, true);
                break;
            case SDL_KEYUP:
                set_button_state(event.key.keysym.sym, false);
                break;
        }
    }
}

void Simulator::render() {
    SDL_SetRenderDrawColor(renderer_, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer_);

    for (size_t i = 0; i < outputs_.displays.size(); ++i) {
        draw(*renderer_, outputs_.displays[i], i);
    }

    draw(*renderer_, outputs_.resistance);
    draw(*renderer_, outputs_.buzzer);
    draw(*renderer_, inputs_.temperature_sensor);
    draw(*renderer_, inputs_.buttons);

    SDL_RenderPresent(renderer_);
}

void Simulator::set_button_state(SDL_Keycode key_code, bool pressed) {
    size_t button_index = 0;

    switch (key_code) {
        case SDLK_a:
            button_index = 0;
            break;
        case SDLK_s:
            button_index = 1;
            break;
        case SDLK_d:
            button_index = 2;
            break;
        default:
            button_index = 999;
            break;
    }

    auto& buttons = inputs_.buttons;

    if (button_index < buttons.size()) {
        auto& button = buttons[button_index];

        if (button.pressed != pressed) {
            SPDLOG_TRACE("Button {} -> {}", button_index, pressed);
        }

        button.pressed = pressed;
    }
}

uint32_t* Simulator::get_registers() {
    return registers_.data();
}

void Simulator::sync_input_registers() {
    uint32_t r = 0;

    auto& buttons = inputs_.buttons;
    r |= (static_cast<uint32_t>(buttons[0].pressed) << 0);
    r |= (static_cast<uint32_t>(buttons[1].pressed) << 1);
    r |= (static_cast<uint32_t>(buttons[2].pressed) << 2);

    r |= (celsius_to_raw_temperature(inputs_.temperature_sensor.celsius) << 3);

    SPDLOG_TRACE("{:08X} {:08X}", registers_[0], registers_[1]);

    registers_[0] = r;
}

void Simulator::sync_output_registers() {
    uint32_t r = registers_[1];

    for (size_t i = 0; i < outputs_.displays.size(); ++i) {
        register_to_seven_seg_display(r, outputs_.displays[i], i);
    }

    outputs_.buzzer.on = (r >> 24) & 1;
    outputs_.resistance.on = (r >> 25) & 1;
}

void Simulator::sync_time_registers() {
    auto now = std::chrono::system_clock::now();
    auto elapsed = now - start_time_;
    auto elapsed_ms = duration_cast<std::chrono::milliseconds>(elapsed).count();
    registers_[2] = elapsed_ms;
}

bool Simulator::should_quit() const {
    return is_quit_requested_;
}

void Simulator::update_temperature() {
    bool oven_is_on = outputs_.resistance.on;
    bool is_above_room_temperature = inputs_.temperature_sensor.celsius > 20.0;

    double temp_diff = 0.0;

    if (oven_is_on) {
        temp_diff = 1.0;
    } else if (is_above_room_temperature) {
        temp_diff = -0.2;
    }

    inputs_.temperature_sensor.celsius += temp_diff;
}

uint32_t celsius_to_raw_temperature(double temperature_celsius) {
    int rounded_temperature = static_cast<int>(std::round(temperature_celsius));

    uint32_t t = (rounded_temperature + 20) / 4;

    if (rounded_temperature < -20) {
        t = 0;
    } else if (rounded_temperature > 492) {
        t = 127;
    }

    return t & 0b1111111;
}

void register_to_seven_seg_display(
    uint32_t r,
    SevenSegmentsDisplay& display,
    uint32_t display_id) {
    uint8_t bits = (r >> (8 * display_id));
    for (int i = 0; i < 8; ++i) {
        display.segments[i] = ((bits >> i) & 1) != 0;
    }
}

void draw(
    SDL_Renderer& renderer,
    const SevenSegmentsDisplay& display,
    uint32_t display_id) {
    static const int display_w = 80;
    static const int margin_left = 20;
    static const int y_offset = 20;

    const int x_offset = margin_left
        + (static_cast<int>(display_id) * (margin_left + display_w));

    auto& segments = display.segments;

    struct Line {
        int x1;
        int y1;
        int x2;
        int y2;
    };

    static const std::array<Line, 7> segment_coordinates{
        Line{0, 0, 1, 0},
        Line{1, 0, 1, 1},
        Line{1, 1, 1, 2},
        Line{0, 2, 1, 2},
        Line{0, 1, 0, 2},
        Line{0, 0, 0, 1},
        Line{0, 1, 1, 1},
    };

    SDL_SetRenderDrawColor(&renderer, 0x99, 0x99, 0xFF, SDL_ALPHA_OPAQUE);

    for (size_t segment_id = 0; segment_id < segment_coordinates.size();
         segment_id++) {
        auto line = segment_coordinates[segment_id];

        if (segments[segment_id]) {
            SDL_RenderDrawLine(
                &renderer,
                x_offset + line.x1 * display_w,
                y_offset + line.y1 * display_w,
                x_offset + line.x2 * display_w,
                y_offset + line.y2 * display_w);
        }
    }

    if (segments[7]) {
        SDL_RenderDrawLine(
            &renderer,
            x_offset + display_w + 5,
            y_offset + display_w * 2,
            x_offset + display_w + 15,
            y_offset + display_w * 2);
    }
}

void draw(SDL_Renderer& renderer, const Buzzer& buzzer) {
    if (buzzer.on) {
        SDL_SetRenderDrawColor(&renderer, 0x00, 0xFF, 0x00, SDL_ALPHA_OPAQUE);
    } else {
        SDL_SetRenderDrawColor(&renderer, 0x33, 0x33, 0x33, SDL_ALPHA_OPAQUE);
    }

    static const SDL_Rect buzzer_rect{380, 50, 50, 50};

    SDL_RenderFillRect(&renderer, &buzzer_rect);
}

void draw(SDL_Renderer& renderer, const Resistance& resistance) {
    if (resistance.on) {
        SDL_SetRenderDrawColor(&renderer, 0xFF, 0x00, 0x00, SDL_ALPHA_OPAQUE);
    } else {
        SDL_SetRenderDrawColor(&renderer, 0x33, 0x33, 0x33, SDL_ALPHA_OPAQUE);
    }

    static const int x_offset = 20;
    static const int x_distance = 40;
    static const int y_min = 250;
    static const int y_max = 450;

    for (int i = 0; i < 12; ++i) {
        SDL_RenderDrawLine(
            &renderer,
            x_offset + (i * x_distance),
            y_min,
            x_offset + (i * x_distance),
            y_max);

        int y = (i % 2) ? y_min : y_max;

        SDL_RenderDrawLine(
            &renderer,
            x_offset + (i * x_distance),
            y,
            x_offset + ((i + 1) * x_distance),
            y);
    }
}

void draw(SDL_Renderer& renderer, const Buttons& buttons) {
    static const SDL_Rect up_rect{320, 150, 50, 50};
    static const SDL_Rect ok_rect{380, 150, 50, 50};
    static const SDL_Rect down_rect{440, 150, 50, 50};
    static const std::array<const SDL_Rect*, 3> rects = {
        &up_rect,
        &ok_rect,
        &down_rect,
    };

    for (int i = 0; i < 3; ++i) {
        if (buttons[i].pressed) {
            SDL_SetRenderDrawColor(
                &renderer,
                0xCC,
                0xCC,
                0xFF,
                SDL_ALPHA_OPAQUE);
        } else {
            SDL_SetRenderDrawColor(
                &renderer,
                0x33,
                0x33,
                0x33,
                SDL_ALPHA_OPAQUE);
        }

        SDL_RenderFillRect(&renderer, rects[i]);
    }
}

void draw(SDL_Renderer& renderer, const TemperatureSensor& temperature_sensor) {
    static constexpr double min = -20.0;
    static constexpr double max = 492.0;
    double temp = temperature_sensor.celsius;
    double clamped = std::clamp(temp, min, max);

    int width = static_cast<int>(500.0 / (max - min) * (clamped - min));

    const SDL_Rect rect{6, 480, width, 40};
    if (temp < 30.0) {
        SDL_SetRenderDrawColor(&renderer, 0x00, 0x66, 0x00, SDL_ALPHA_OPAQUE);
    } else if (temp < 100.0) {
        SDL_SetRenderDrawColor(&renderer, 0xFF, 0xFF, 0x33, SDL_ALPHA_OPAQUE);
    } else if (temp < 300.0) {
        SDL_SetRenderDrawColor(&renderer, 0xFF, 0x66, 0x33, SDL_ALPHA_OPAQUE);
    } else if (temp < 492.0) {
        SDL_SetRenderDrawColor(&renderer, 0xFF, 0x00, 0x00, SDL_ALPHA_OPAQUE);
    } else {
        SDL_SetRenderDrawColor(&renderer, 0x33, 0x00, 0x00, SDL_ALPHA_OPAQUE);
    }

    SDL_RenderFillRect(&renderer, &rect);
}

}  // namespace simulator