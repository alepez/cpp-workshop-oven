#include <spdlog/spdlog.h>

#include "simulator.hpp"

using namespace simulator;

void example(uint32_t* registers) {
    // 3 32bits registers are available
    // registers[0] is input register
    // registers[1] is output register
    // registers[2] is time since start in milliseconds

    // Read input registers
    // Lowest 3 bits (bits [0-2]) are bound to buttons
    // Simulator uses keyboard A S D buttons for Up, Ok, Down
    bool is_up_pressed = ((registers[0] >> 0) & 1) != 0;
    bool is_ok_pressed = ((registers[0] >> 1) & 1) != 0;
    bool is_down_pressed = ((registers[0] >> 2) & 1) != 0;

    // Temperature is available as a 7bits register.
    // bits [3-9]
    // 0 equals to -20 Celsius
    // Unit is 4 Celsius
    uint32_t temperature = (((registers[0] >> 3) & 0b1111111) * 4) - 20;

    // Heat resistance is bound to output register at bit 25
    // Here, as an example, we set the bit high when ok button is pressed
    registers[1] = (registers[1] & ~(1UL << 25))
        | (static_cast<uint32_t>(is_ok_pressed) << 25);

    // Buzzer is bound to output register at bit 24
    // Here, as an example, we set the bit high when up button is pressed
    registers[1] = (registers[1] & ~(1UL << 24))
        | (static_cast<uint32_t>(is_up_pressed) << 24);

    // Three seven segment displays are bound to bits [0-23]
    // Each seven segment display is bound to 8 bits, as drawn in the
    // figure below:
    //
    //    0
    //    -
    // 5 | | 1
    //    _  6
    // 4 | | 2
    //    - -
    //    3 7
    //
    // Bits 0-6 are segments, bit 7 is the decimal point.
    registers[1] |= (0b10000110) << 0;  // Display digit 1.
    registers[1] |= (0b01011011) << 8;  // Display 2
    registers[1] |= (0b01001111) << 16;  // Display 3

    auto elapsed_ms = registers[2];

    SPDLOG_INFO(
        "{} {} {} {} {}",
        is_up_pressed,
        is_ok_pressed,
        is_down_pressed,
        temperature,
        elapsed_ms);
}

int main(int argc, const char** argv) {
    spdlog::set_level(spdlog::level::debug);

    SPDLOG_DEBUG("Oven simulator");

    (void)argc;
    (void)argv;

    Simulator simulator;

    uint32_t* registers = simulator.get_registers();

    while (!simulator.should_quit()) {
        simulator.update();

        // TODO Integrate application code here

        // TODO Remove example code below
        example(registers);
    }

    return 0;
}
