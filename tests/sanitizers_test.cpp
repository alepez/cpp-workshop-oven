#include <array>
#include <catch2/catch_test_macros.hpp>

double division(double x, double y) {
    return x / y;
}

double get_item(double* arr, int index) {
    return arr[index];
}

TEST_CASE("ubsan", "[.ubsan_should_fail]") {
    // ubsan should detect a division by zero
    auto result = division(1.0, 0.0);
    REQUIRE(result != 42.0);
}

TEST_CASE("Invalid memory access", "[.asan_should_fail]") {
    // asan should detect an invalid memory access
    std::array<double, 2> arr = {1.0, 2.0};
    auto data = &arr[0];
    int index = 2 + (rand() % 2);
    auto result = get_item(data, index) != 0.0;
    REQUIRE(result != 42.0);
}

TEST_CASE("Uninitialized value, leaked", "[.msan_should_fail]") {
    // msan should detect a memory leak
    auto x_ptr = new double;
    REQUIRE(*x_ptr != 42.0);
    *x_ptr = 42.0;
    delete x_ptr;
    REQUIRE(*x_ptr == 42.0);
}
