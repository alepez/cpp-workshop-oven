Include(FetchContent)

FetchContent_Declare(
        fmt
        URL https://github.com/fmtlib/fmt/archive/refs/tags/9.1.0.zip
        URL_HASH SHA1=f630891a844fb7ecd71ee37fdcd946eac3c8814b
)

FetchContent_MakeAvailable(fmt)