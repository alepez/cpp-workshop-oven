Include(FetchContent)

FetchContent_Declare(
        Catch2
        URL https://github.com/catchorg/Catch2/archive/refs/tags/v3.0.1.zip
        URL_HASH SHA1=fa7183b44b7a584eed0265426a690671112c52d5
)

FetchContent_MakeAvailable(Catch2)