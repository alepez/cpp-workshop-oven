Include(FetchContent)

FetchContent_Declare(
        SDL2
        URL https://github.com/libsdl-org/SDL/archive/refs/tags/release-2.26.0.zip
        URL_HASH SHA1=b8bb4453cd0d9e4b0c5e983e6af84ed3f12ad7c7
)

FetchContent_MakeAvailable(SDL2)
